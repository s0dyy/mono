# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2013 Aleksandar Petrinic <petrinic@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="An Open Source implementation of the GDI+ API"
HOMEPAGE="https://www.mono-project.com/"
DOWNLOADS="https://download.mono-project.com/sources/${PN}/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    exif
    gif
    tiff
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.2.3]
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libpng:=[>=1.6]
        x11-libs/cairo[>=1.6.5]
        x11-libs/harfbuzz
        x11-libs/libX11
        x11-libs/libXrender
        x11-libs/pango[>=1.40.14]
        exif? ( media-libs/libexif )
        gif? ( media-libs/giflib:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff[providers:ijg-jpeg?][providers:jpeg-turbo?] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-pango
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'exif libexif'
    'gif libgif'
    'tiff libtiff'
)

src_prepare() {
    # Theoretically, overrides are possible - in practice, "pkg-config" is hardcoded...
    # TODO seems to be fixed in master
    edo sed -i -e "s:pkg-config:$(exhost --tool-prefix)pkg-config:g" configure.ac

    # broken in CI tests or possibly disabled features
    edo sed -E \
        -e "/TESTS/,/NULL/ {
            /test(codecs|font|gifcodec|gpimage|icocodec|text)/ d }" \
        -e "/testtiffcodec/ d" \
        -i tests/Makefile.am

    autotools_src_prepare
}

src_test() {
    esandbox allow_net --connect "LOCAL@6000"
    DISPLAY= \
        default
    esandbox disallow_net --connect "LOCAL@6000"
}


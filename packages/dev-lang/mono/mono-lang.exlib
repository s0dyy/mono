# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the temrs of the GNU General Public License v2

require mono autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

export_exlib_phases src_prepare src_configure src_test src_install pkg_postinst pkg_postrm

SUMMARY="An opensource implementation of the .NET framework"
HOMEPAGE="https://www.mono-project.com/"
DOWNLOADS="https://download.mono-project.com/sources/${PN}/${PNV}.tar.bz2"

LICENCES="( GPL-2 LGPL-2 X11 )"
SLOT="0"
MYOPTIONS="X"

DEPENDENCIES="
    build:
        app-misc/ca-certificates
        sys-apps/bc
        sys-devel/bison
        sys-devel/gettext[>=0.10.35]
        sys-libs/zlib
        virtual/pkg-config[>=0.20]
    build+run:
        X? (
            dev-dotnet/libgdiplus[>=2]
            x11-libs/libX11
        )
"

mono-lang_src_prepare() {
    edo sed -e "s/AC_PATH_PROG(PKG_CONFIG, pkg-config, no)/PKG_PROG_PKG_CONFIG/" -i configure.ac

    # make aot-compiler use prefixed 'as'
    edo sed \
        -e "s/\(--aot=\)/\1tool-prefix=$(exhost --tool-prefix),/" \
        -i mcs/class/aot-compiler/Makefile

    autotools_src_prepare
}

mono-lang_src_configure() {
    if ! option X ; then
        export X11=libX11.so
    fi

    econf \
        --enable-boehm \
        --enable-parallel-mark \
        --enable-system-aot \
        --disable-llvm \
        --disable-vtune \
        --with-gc=included \
        --with-libgdiplus=$(option X && echo 'installed' || echo 'no')  \
        --with-profile2 \
        --with-profile4 \
        --with-profile4_5 \
        --with-sgen \
        --without-ikvm-native \
        --without-mobile_static \
        --without-monodroid \
        --without-monotouch \
        --without-monotouch_tv \
        --without-monotouch_watch \
        --without-moonlight \
        --without-xammac \
        --without-xen_opt \
        $(option_with X x)
}

mono-lang_src_test() {
    edo pushd mono/tests
    # Disable failing tests (last checked: 4.6.1.3)
    EXTRA_DISABLED_TESTS="appdomain-unload-doesnot-raise-pending-events.exe \
        delegate2.exe unload-appdomain-on-shutdown.exe" emake check
    edo popd

    edo pushd mcs/tests
    emake check
    edo popd
}

mono-lang_src_install() {
    default
    exeinto /etc/ca-certificates/update.d
    doexe "${FILES}"/mono-cert-sync
}

mono-lang_pkg_postinst() {
    # mono has it's own way of dealing with certificates
    # this is also required so nuget works correctly
    einfo "Creating the global mono certificate store"
    einfo "in /usr/share/.mono/certs"
    einfo "from /etc/ssl/certs/ca-certificates.crt."
    cert-sync --quiet /etc/ssl/certs/ca-certificates.crt
}

mono-lang_pkg_postrm() {
    if [[ -z ${REPLACED_BY_ID} ]] ; then
        elog "The global mono certificate store at"
        elog "  /usr/share/.mono"
        elog "might still contain untracked files."
    fi
}

